using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CapsuleRotation : MonoBehaviour
{   
    //Var 
    [SerializeField]
    private Vector3 rotation;
    public float speed;

    void Update()
    {
       rotation = ClampVector3(rotation);

       transform.Rotate(rotation * (speed * Time.deltaTime));
    }
    //VC
    public static Vector3 ClampVector3(Vector3 target)
    {
        float clampedX = Mathf.Clamp(target.x, -1f, 1f);

        float clampedY = Mathf.Clamp(target.y, 0f, 0f);

        float clampedZ = Mathf.Clamp(target.z, 0f, 0f);

        Vector3 result = new Vector3(clampedX, clampedY, clampedZ);

        return result;
    }
}
