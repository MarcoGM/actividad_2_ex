using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerFPSController1 : MonoBehaviour
{
    // Start is called before the first frame update

    public GameObject camara;
    public float walkSpeed = 5f;
    public float hRotationSpeed = 100f;
    public float vRotationSpeed = 80f;


    void Start()
    {
        Cursor.visible = false;
        Cursor.lockState = CursorLockMode.Locked;

        GameObject.Find("Capsule").gameObject.SetActive(false);

    }

    // Update is called once per frame
    void Update()
    {

        movement();

    }

    private void movement()
    {
        //Movimiento
        float hMovement = Input.GetAxisRaw("Horizontal");
        float vMovement = Input.GetAxisRaw("Vertical");

        Vector3 movementDirection = hMovement * Vector3.right + vMovement * Vector3.forward;
        transform.Translate(movementDirection * (walkSpeed * Time.deltaTime));


        //Rotacion
        float vCamRotation = Input.GetAxis("Mouse Y") * vRotationSpeed * Time.deltaTime;
        float hPlayerRotation = Input.GetAxis("Mouse X") * hRotationSpeed * Time.deltaTime;

        transform.Rotate(0f, hPlayerRotation, 0f);
        camara.transform.Rotate(-vCamRotation, 0f, 0f);
    }
}
